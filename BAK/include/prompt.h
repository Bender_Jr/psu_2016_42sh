/*
1;4601;0c** prompt.h for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sat Apr 15 08:03:08 2017 Bender_Jr
** Last update Thu May 11 17:12:25 2017 Bender_Jr
*/

#ifndef PROMPT_H_
# define PROMPT_H_

/*
** typdefs :
*/
# include "decl.h"

/*
** size_t typedef
*/
typedef unsigned long size_t;

/*
** max path len (to be moved
** in limits.h
*/
#ifndef PATH_SIZE
# define PATH_SIZE (4096)
# endif /* !PATH_SIZE */

/*
** src/prompt_print/
*/
void	fill_ptr(flags_fct ptr[]);
int	fill_promptbfr(const char flag);
int	pr_printf(const char *format);

/*
** Gnu fonction prototyping
*/
char	*secure_getenv(const char *name);
char	*getcwd(char *buf, size_t size);

#endif /* !PROMPT_H_ */
