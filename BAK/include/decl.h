/*
** decl.h for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sun Apr 23 11:01:17 2017 Bender_Jr
** Last update Mon May 15 11:59:10 2017 Bender_Jr
*/

#ifndef DECL_H_
# define DECL_H_

/*
** this header contain all the struct
** and typedef declaration of the shell
*/

/*
** for debug and signal handler context
*/
# define UNUSED __attribute__((unused))

/*
** check_str valid char
*/
#ifndef LEGIT_CHAR
# define LEGIT_CHAR "0123456789_ABCDEFGHIJKLMNOPQRSTUVWXYZ \
abcdefghijklmnopqrstuvwxyz\
!#,:`'*.=$-+/\\()?%~>\"@]["
# endif /* !LEGIT_CHAR */

#ifndef LEGIT_VARCHAR
#define  LEGIT_VARCHAR "0123456789_ABCDEFGHIJKLMNOPQRSTUVWXYZ \
-abcdefghijklmnopqrstuvwxyz"
# endif /* !LEGIT_VARCHAR */

/*
** to quick check
*/
# define L_CHAR(a)	(a >= 32 && a<= 126) ? 1 : 0

/*
** Limits values
*/
#ifndef PATH_MAX
# define PATH_MAX (4096)
# endif /* !PATH_MAX */

/*
** return value volatile glob var
*/
int volatile	g_rt;

/*
** exit bool
*/
int volatile    g_exit;

/*
**             =========
**        42sh mains structs
**         ****************
*/
typedef struct	s_vars {
  struct s_vars	*prev;
  struct s_vars	*next;
  char		*bin_name;
  char		bin_path[PATH_MAX];
  unsigned long	chksum;
}		t_vars;

typedef struct	s_path {
  char		*path_var;
  t_vars	*firstvar;
  t_vars	*lastvar;
}		t_path;

/*
** here comes the command parser
** binary tree (the 2 union corresponding to
** the left and right branch)
*/

typedef struct		s_wordent {
  char			*word;
  int			order;
  struct s_wordent	*prev;
  struct s_wordent	*next;
}			t_wordent;

typedef struct		s_command {
  union {
    char		*r_output;
    struct s_command	*right_nodes;
  } R;
  union {
    char		*l_output;
    struct s_command	*left_nodes;
  } L;
  t_wordent		*head;
  t_wordent		*tail;
}			t_command;

/*
** dictionnary struicture (lexing symbols table)
*/
typedef struct		s_lex {
  char			symb[3];
  unsigned long		sum;
  int			prio;
}			t_lex;

/*
** defines for histfile
*/
#ifndef HIST_FILE
# define HIST_FILE	".42history"
# endif /* !HIST_FILE */

#ifndef HF_FLAG
#define HF_FLAG		(O_RDWR | O_CREAT | O_APPEND)
# endif /* !HF_FLAG */

#ifndef HF_MODE
# define HF_MODE	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
# endif /* !HF_MODE */

typedef struct	s_hist {
  char		*cmd;
  char		*timestamp;
  int		order;
  struct s_hist	*next;
  struct s_hist	*prev;
}		t_hist;

typedef struct	s_history {
  t_hist	*histfirst;
  t_hist	*histlast;
  int		histfilefd;
}		t_history;

/*
** Env macro if "virgin" extern env
*/
#ifndef SH_ENV
# define SH_ENV	"PATH=/bin:/usr/bin,SHELL=42sh"
# endif /* !SH_ENV */

typedef struct		s_envar {
  char			*variable;
  char		        value[PATH_MAX];
  unsigned long		hash;
  struct s_envar	*prev;
  struct s_envar	*next;
}			t_envar;

typedef struct		s_environ {
  char			**environ;
  t_envar		*firstenvar;
  t_envar		*lastenvar;
}			t_environ;

/*
** status message and rt val handler
*/
typedef struct	s_status {
  int		rt;
  char		msg[42];
}		t_status;

/*
** for pid_t type
*/
typedef int		pid_t;
typedef struct		s_exec {
  char			*cmdpath;
  unsigned long		cksum;
  pid_t			child_pid;
  pid_t			parentpid;
  int			status;
}			t_exec;

/*
** for struct termios typedef
*/
# include <termios.h>

/*
** note : the prompt format
** will be move in the conf file
** later on
*/
typedef struct		s_termios {
  struct termios	new;
  struct termios	save;
  const char		*prompt_frmat;
  int			tty_fd;
}			t_termios;

/*
** Builtins Macro and Typedef
** develloped then named
*/
#ifndef BLTS_NAMES
# define BLTS_NAMES "cd,echo,exit,env,setenv,unsetenv,clear"
# endif /* !BLTS_NAMES */

typedef int		(*builtins)(char **bfr, void *ptr);
typedef struct		s_blts {
  char			**blts_names;
  builtins		btptr[7];
}			t_blts;

typedef struct		s_shell {
  t_termios		term;
  t_blts		blts;
  t_path		*pathlist;
  t_history		*history;
  t_environ		*envlist;
  char			**environ;
}			t_shell;

/*
** Prompt size define
*/
#ifndef PROMPT_SIZE
# define PROMPT_SIZE (126)
# endif /* !PROMPT_SIZE */

/*
** prompt glob_var
*/
char	g_prompt[PROMPT_SIZE];

/*
** prompt_printf(fction ptr)
*/
typedef int (*flags_fct)(char prompt_bfr[]);
extern flags_fct ptr[];

/*
** prompt_printf flags list
*/
#ifndef PRPT_FLAGS
# define PRPT_FLAGS "HUT~C"
# endif /* !PRPT_FLAGS */

/*
** Flag '%H' for hostname
*/
int	set_hostname(char prompt_bfr[]);

/*
** Flag '%U' for user
*/
int	set_username(char prompt_bfr[]);

/*
** Flag '%T' for current time
*/
int	set_time(char prompt_bfr[]);

/*
** Flag '%~' to shorten home path
** Note : this flag print the
** current working dir minus the /home/$user
** path (replaced by '~' char)
*/
int	set_cwd(char prompt_bfr[]);

/*
** Flag '%C' for setting special
** color when root shell
*/
int	set_usertype(char prompt_bfr[]);

#endif /* !DECL_H_ */
