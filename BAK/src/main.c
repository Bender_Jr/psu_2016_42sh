/*
** main.c for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri Apr 14 21:35:39 2017 Bender_Jr
** Last update Thu May 11 17:07:52 2017 Bender_Jr
*/

# include <stdlib.h>
# include "base.h"

int			main(void)
{
  t_shell		tonton;
  char			*path;
  extern char		**environ;

  tonton.term.prompt_frmat = "\033[0m%U@%H \033[1m%~\033[0m >> ";
  fill_builtins(&(tonton).blts);
  environ = strto_wordtab(SH_ENV, ",");
  tonton.environ = environ;
  path = xgetenv("PATH");
  if ((tonton.envlist = init_environ(environ)) == NULL ||
      (tonton.history = init_history()) == NULL ||
      (g_rt = init_term(&(tonton).term)) == -1 ||
      (tonton.pathlist = init_paths(path)) == NULL ||
      (tonton.pathlist = fill_path(tonton.pathlist)) == NULL)
    return (free(path), 1);
  run(&tonton);
  return (free(path), g_rt);
}
