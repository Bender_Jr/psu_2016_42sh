/*
** run_exec.c for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sun Apr 23 10:17:57 2017 Bender_Jr
** Last update Tue May  2 20:15:24 2017 Bender_Jr
*/

/*
** for strerror, errno free, excve etc
*/
# include <string.h>
# include <errno.h>
# include <stdlib.h>
# include "base.h"
# include "get_next_line.h"

int		clean_exit(t_shell *ptr)
{
  freetab(ptr->blts.blts_names);
  free_list(ptr->pathlist);
  close(ptr->history->histfilefd);
  free_history(ptr->history);
  free_env(ptr->envlist);
  reset_cap(&(ptr)->term.save, ptr->term.tty_fd);
  freetab(ptr->environ);
  return (g_rt = (g_rt == 1 && g_exit == 1) ? 0 : g_rt);
}

char		**get_mybfr(t_shell *ptr, char *tmp, char *err)
{
  char		**bfr;

  bfr = strto_wordtab(tmp, " ");
  (bfr[1]) ? my_strcpy(err, bfr[1]) : my_strcpy(err, bfr[0]);
  if ((ptr->history = fill_history(ptr->history, tmp)) == NULL ||
      (g_rt = is_builtins(bfr, ptr, &(ptr)->blts)) == -1 ||
      (!g_rt && (g_rt = exec(bfr, ptr)) == -1))
    return (NULL);
  return (bfr);
}

int		run(t_shell *ptr)
{
  char		*tmp;
  char		**bfr;
  char		err[PATH_MAX];


  g_exit = 0;
  while ((tmp = get_next_line(ptr->term.tty_fd)))
    {
      g_rt = 0;
      xmemset(err, '\0', sizeof(err));
      if (len(tmp) && !(tmp[0] >= 0 && tmp[0] <= 31))
	if ((is_legitstr(tmp = epurstr(tmp, ' '), LEGIT_CHAR)) >= 0)
	  {
	    if ((bfr = get_mybfr(ptr, tmp, err)) == NULL)
	      (errno && errno != 25) ? p_printf(2, "%s: %s.\n", err, strerror(errno)) :
		(errno);
	    else if (g_exit >= 1)
	      return (free(tmp), clean_exit(ptr));
	  }
      g_rt = (g_rt == -1) ? 1 : g_rt == 1 ? 0 : g_rt;
      pr_printf(ptr->term.prompt_frmat);
      free(tmp);
    }
  return (g_rt = clean_exit(ptr));
}
