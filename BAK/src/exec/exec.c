/*
** exec.c for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Tue May  2 19:36:46 2017 Bender_Jr
** Last update Wed May 17 23:45:54 2017 corsen_p
*/

/*
** for exit and xaccess stat
*/
# include <sys/stat.h>
# include <stdlib.h>
# include <errno.h>

# include "base.h"

int			xaccess(const char *pathname, UNUSED int mode)
{
  struct stat		sb;

  if (!stat(pathname, &sb))
    {
      if (S_ISDIR(sb.st_mode))
	return (-1);
      else if (S_ISREG(sb.st_mode) && (sb.st_mode & 0111))
	return (0);
    }
  return (-1);
}

int		rt_cmd_not_found(char **argv)
{
  errno = 0;
  p_printf(2, "%s: %s\n", argv[0], "Command not found.");
  freetab(argv);
  return (-1);
}

int		exec(char **argv, t_shell *ptr)
{
  t_exec	execl;

  g_rt = 0;
  xmemset(&execl, '\0', sizeof(execl));
  execl.cksum = get_sum((unsigned char *)argv[0]);
  if ((execl.cmdpath = is_proginlist(ptr->pathlist, execl.cksum)) == NULL &&
      (access(argv[0], X_OK)) == -1)
    return (rt_cmd_not_found(argv));
  if  ((execl.parentpid = getpid()) == -1 ||
       (execl.child_pid = fork()) == -1)
    return (freetab(argv), -1);
  else if (execl.child_pid == 0)
    {
      !xaccess(argv[0], 1) ? execl.cmdpath = argv[0] : execl.cmdpath;
      if ((execve(execl.cmdpath, argv, ptr->environ)))
	{
	  p_printf(2, "%s: %s\n", argv[0], "Permission denied.");
	  g_rt = 0;
	  exit (1);
	}
    }
  else
    check_status(execl.child_pid, &(execl).status);
  return (freetab(argv), g_rt);
}
