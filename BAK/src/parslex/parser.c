/*
** parser.c for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Wed Apr 26 11:38:23 2017 Bender_Jr
** Last update Wed Apr 26 13:28:56 2017 Bender_Jr
*/

# include <stdlib.h>
# include "get_next_line.h"
# include "base.h"

typedef struct		s_wordent {
  char			*word;
  struct s_wordent	*prev;
  struct s_wordent	*next;
}			t_wordent;

typedef struct		s_command {
  unsigned char		cmd_type;
  t_wordent		*first;
  t_wordent		*last;
}			t_command;

t_wordent		*get_word(char *buff)
{
  t_wordent		*new_word;

  if ((new_word = malloc(sizeof(*new_word))) == NULL)
    return (NULL);
  new_word->word = my_strdup(buff);
  new_word->prev = NULL;
  new_word->next = NULL;
  return (new_word);
}

t_command		*get_command(t_command *cmd, char **buff)
{
  t_wordent		*new_word;
  int			i;

  i = 0;
  while (i != tab_len(buff))
    {
      if ((new_word = get_word(buff[i])) == NULL)
	return (NULL);
      if (cmd->first == NULL)
	{
	  cmd->first = new_word;
	  cmd->last = new_word;
	}
      else
	{
	  new_word->prev = cmd->last;
	  cmd->last->next = new_word;
	  cmd->last = new_word;
	}
      i++;
    }
  return (cmd);
}

t_command	*init_cmd()
{
  t_command	*new;

  if ((new = malloc(sizeof(*new))) == NULL)
    return (NULL);
  new->first = NULL;
  new->last = NULL;
  return (new);
}

void		print_cmd(t_command *cmd)
{
  t_wordent	*tmp;

  tmp = cmd->first;
  while (tmp)
    {
      p_printf(1, "[%s]\n", tmp->word);
      tmp = tmp->next;
    }
}

int		main(void)
{
  t_command	*ptr;
  char		**tmp;
  char		*bfr;

  ptr = init_cmd();
  while ((bfr = get_next_line(0)))
    {
      tmp = strto_wordtab(bfr, " ");
      ptr = get_command(ptr, tmp);
      print_cmd(ptr);
      free(bfr);
    }
  return (0);
}
