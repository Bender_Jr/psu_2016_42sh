/*
** scripting.c for PSU_2016_42sh in /home/bender/42sh/work/testparsing
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sun Apr 30 07:51:23 2017 Bender_Jr
** Last update Mon May 15 17:55:14 2017 Bender_Jr
*/

/*
** for termios funct
*/
# define _DEFAULT_SOURCE
# define MAX_BUFF (1024)
# define CLR "\033[H\033[2J"
# define PRMPT ">> "
# include <termios.h>
# include <stdlib.h>
# include <unistd.h>
# include "get_next_line.h"
# include "base.h"

void		fill_right_nodes(t_command *ptr, t_wordent *new)
{
  if (ptr->R.right_nodes->head == NULL)
    {
      ptr->R.right_nodes->head = new;
      ptr->R.right_nodes->tail = new;
    }
  else
    {
      new->prev = ptr->R.right_nodes->tail;
      ptr->R.right_nodes->tail->next = new;
      ptr->R.right_nodes->tail = new;
    }
}


void		fill_left_nodes(t_command *ptr, t_wordent *new)
{
  if (ptr->L.left_nodes->head == NULL)
    {
      ptr->L.left_nodes->head = new;
      ptr->L.left_nodes->tail = new;
    }
  else
    {
      new->prev = ptr->L.left_nodes->tail;
      ptr->L.left_nodes->tail->next = new;
      ptr->L.left_nodes->tail = new;
    }
}

t_command		*get_command(t_command *ptr, char *line)
{
  t_wordent		*new;
  static int		redir = 0;

  if ((new = get_words(line)) == NULL)
    return (NULL);
  redir += (new->order < 4) ? 0 : 1;
  if (!redir)
    fill_right_nodes(ptr, new);
  else if (redir)
    fill_left_nodes(ptr, new);
  return (ptr);
}

t_command	*get_dirnode()
{
  t_command	*nodes_dir;

  if ((nodes_dir = malloc(sizeof(*nodes_dir))) == NULL)
    return (NULL);
  nodes_dir->head = NULL;
  nodes_dir->tail = NULL;
  return (nodes_dir);
}

void			free_word(t_wordent *tmp)
{
  while (tmp)
    {
      free(tmp->word);
      free(tmp->prev);
      tmp = tmp->next;
    }
}

void			free_cmd(t_command *ptr)
{
  if (ptr->R.right_nodes->head)
    {
      free_word(ptr->R.right_nodes->head);
      free(ptr->R.right_nodes->tail);
    }
  if (ptr->L.left_nodes->head)
    {
      free_word(ptr->L.left_nodes->head);
      free(ptr->L.left_nodes->tail);
    }
  free(ptr->R.right_nodes);
  free(ptr->L.left_nodes);
}

/*
** ?Note :
** faut faire un pointeur sur fonction la
** un truc en int (test*)(int c)
*/
void			handle_esc(char test[], char line[], ssize_t rd)
{
  unsigned long		sum;

  sum = get_sum((unsigned char *)test);
  if ((test[0] == '\033'))
    {
      (sum == get_sum((unsigned char *)"\033[A")) ?
	p_printf(1, "\r%shist_prev\r", PRMPT) :
	(sum == get_sum((unsigned char *)"\033[B")) ?
	p_printf(1, "\r%shist_next\r", PRMPT) :
	(sum == get_sum((unsigned char *)"\033[C")) ?
	p_printf(1, "\033[C") :
	(sum == get_sum((unsigned char *)"\033[D")) ?
	p_printf(1, "\033[D") : rd;
      xmemset(test, '\0', 8);
    }
  else if ((test[0] == '\n') && isatty(STDIN_FILENO))
    p_printf(1, "\n>> ");
  else if (test[0] == 9)
    {
      my_strcatvs(line, " ");
      p_printf(1, "\t");
    }
  else if (test[0] == 127)
    p_printf(1, "\b");
}

int	wait_forit(char line[], t_command list)
{
  char *line_ptr;

  line_ptr = NULL;
  if (((line_ptr = my_strdup(line))) == NULL ||
      (line_ptr = epurstr(line_ptr, ' ')) == NULL)
    return (-1);
  get_command(&list, line_ptr);
  xmemset(line, '\0', 4096);
  free(line_ptr);
  return (0);
}

void	xsetterm_opt(struct termios *new, struct termios *old)
{
  if (isatty(STDIN_FILENO))
    {
      tcgetattr(STDIN_FILENO, old);
      tcgetattr(STDIN_FILENO, new);
      new->c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | NOFLSH);
      new->c_lflag |= (ISIG);
      new->c_cflag |= CS8;
      tcsetattr(STDIN_FILENO, TCSANOW, new);
    }
}

int	        get_line(t_command list)
{
  ssize_t	rd;
  char		test[1];
  char		line[MAX_BUFF];

  rd = 0;
  while ((rd = read(STDIN_FILENO, test, sizeof(test))) != -1)
    {
      test[rd] = '\0';
      usleep(130);
      if ((!isatty(STDIN_FILENO) && test[0] == 0) ||
	  test[0] == 4 )
	return (0);
      (L_CHAR(test[0]) && isatty(STDIN_FILENO)) ?
	p_printf(1, "%s", test) : rd;
	/* (test[0] == '\n' || rd == 0) ? */
	/* wait_forit(line, list) : rd; */
      handle_esc(test, line, rd);
      /* my_strcatvs(line, test) */
      (test[0] && L_CHAR(test[0])) ?
	get_command(&list, test) : &list;
    }
  return (0);
}

int			main(void)
{

  struct termios	old;
  struct termios	new;
  t_command		list;

  if ((list.R.right_nodes = get_dirnode()) == NULL ||
      (list.L.left_nodes = get_dirnode()) == NULL)
    return (84);
  xsetterm_opt(&new, &old);
  if (isatty(STDIN_FILENO))
    p_printf(1, ">> ");
  get_line(list);
  print_cmd(&list);
  free_cmd(&list);
  tcsetattr(STDIN_FILENO, TCSANOW, &old);
  return (0);
}
