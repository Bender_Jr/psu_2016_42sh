/*
** free_tree.c for PSU_2016_42sh in /home/bender/Repo/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Mon May 15 12:22:13 2017 Bender_Jr
** Last update Mon May 15 12:24:34 2017 Bender_Jr
*/

# include <stdlib.h>
# include "base.h"

void			free_word(t_wordent *tmp)
{
  while (tmp)
    {
      free(tmp->word);
      free(tmp->prev);
      tmp = tmp->next;
    }
}

void			free_cmd(t_command *ptr)
{
  if (ptr->R.right_nodes->head)
    {
      free_word(ptr->R.right_nodes->head);
      free(ptr->R.right_nodes->tail);
    }
  if (ptr->L.left_nodes->head)
    {
      free_word(ptr->L.left_nodes->head);
      free(ptr->L.left_nodes->tail);
    }
  free(ptr->R.right_nodes);
  free(ptr->L.left_nodes);
}
