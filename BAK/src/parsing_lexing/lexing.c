/*
** lexing.c for PSU_2016_42sh in /home/bender/Repo/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Mon May 15 12:05:31 2017 Bender_Jr
** Last update Mon May 15 12:06:07 2017 Bender_Jr
*/

# include <stdlib.h>
# include "base.h"

t_lex	*fill_voc(t_lex voc[])
{
  int	i;
  char	**tmp;

  i = 0;
  tmp = strto_wordtab("(,),~,|,||,>,<,>>,<<,;,\"", ",");
  while (i != tab_len(tmp))
    {
      my_strcpy(voc[i].symb, tmp[i]);
      voc[i].prio = i + 1;
      voc[i].sum = get_sum((unsigned char *)voc[i].symb);
      i++;
    }
  voc[i].prio = 0xcaca;
  freetab(tmp);
  return (voc);
}

int			get_order(t_lex voc[], const char *word)
{
  unsigned long		chk;
  int			i;

  i = 0;
  chk = get_sum((unsigned char *)word);
  while (voc[i].prio != 0xcaca)
    {
      if (voc[i].sum == chk)
	return (voc[i].prio);
      i++;
    }
  return (0);
}

t_wordent		*get_words(const char *word)
{
  t_wordent		*new;
  t_lex			voc[27];

  fill_voc(voc);
  if ((new = malloc(sizeof(*new))) == NULL)
    return (NULL);
  new->word = my_strdup(word);
  new->order = get_order(voc, word);
  new->prev = NULL;
  new->next = NULL;
  return (new);
}
