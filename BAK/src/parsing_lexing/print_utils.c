/*
** print_utils.c for PSU_2016_42sh in /home/bender/Repo/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Mon May 15 12:04:04 2017 Bender_Jr
** Last update Mon May 15 12:04:06 2017 Bender_Jr
*/

# include <stdlib.h>
# include "base.h"

void			print_wrd(t_wordent *tmp)
{
  while (tmp)
    {
      if (isatty(STDIN_FILENO))
	p_printf(1, "[%d] : [%s]\n", tmp->order, tmp->word);
      tmp = tmp->next;
    }
}

void			print_cmd(t_command *ptr)
{
  t_wordent		*tmp;

  if (ptr->R.right_nodes->head)
    {
      tmp = ptr->R.right_nodes->head;
      p_printf(1, "\nR\n");
      print_wrd(tmp);
    }
  if (ptr->L.left_nodes->head)
    {
      tmp = ptr->L.left_nodes->head;
      p_printf(1, "\nL\n");
      print_wrd(tmp);
    }
}
