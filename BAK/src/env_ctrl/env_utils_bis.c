/*
** env_utils_bis.c for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May  5 09:23:07 2017 Bender_Jr
** Last update Sat May  6 09:00:46 2017 Bender_Jr
*/

# include <stdlib.h>
# include "base.h"

t_environ	*clear_nodes(t_environ *ptr, char **toremove)
{
  size_t	i;
  t_envar	*tmp;
  size_t	ac;
  unsigned long	sum;

  i = 1;
  ac = tab_len(toremove);
  tmp = ptr->firstenvar;
  p_printf(1, "coucou\n");
  while (i != ac)
    {
      sum = get_sum((unsigned char *)toremove[i]);
      while (ptr->firstenvar)
	{
	  if (ptr->lastenvar->hash == sum)
	    {
	      free(ptr->firstenvar->variable);
	      ptr->firstenvar->next = ptr->firstenvar->prev;
	      free(ptr->firstenvar->prev);
	    }
	  ptr->firstenvar = ptr->firstenvar->next;
	}
      i++;
    }
  ptr->firstenvar = tmp;
  return (ptr);
}
