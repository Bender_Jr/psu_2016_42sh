/*
** xgetenv.c for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Tue May  2 18:53:11 2017 Bender_Jr
** Last update Mon May 15 16:51:21 2017 Romain LUGAS
*/

# include "base.h"

char		*xgetenv(const char *name)
{
  int		i;
  char		*cpy;
  extern char	**environ;

  i = 0;
  cpy = NULL;
  while (i != tab_len(environ))
    {
      if ((strn_cmp(name, environ[i], len(name))))
	return (cpy = my_strdup(environ[i] + len(name) + 1));
      i++;
    }
  return (cpy);
}
