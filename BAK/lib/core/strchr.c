/*
** strchr.c for PSU_2016_42sh in /home/bender/Repo/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Mon May 15 15:54:09 2017 Bender_Jr
** Last update Mon May 15 15:54:11 2017 Bender_Jr
*/

# include "test.h"

char		*xstrchr(const char *s, int c)
{
  size_t	i;
  char		*pos;

  i = 0;
  while (s[i])
    {
      pos = (char *)&s[i];
      if (c == *pos)
	return (pos);
      i++;
    }
  return (NULL);
}

char		*xstrrchr(const char *s, int c)
{
  size_t	i;
  char		*pos;

  i = strlen(s);
  while (i)
    {
      pos = (char *)&s[i];
      if (c == *pos)
	return (pos);
      i--;
    }
  return (NULL);
}
