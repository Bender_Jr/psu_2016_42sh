##
## READ_ME.txt for 42 in /home/boissi_p/rendu/test
## 
## Made by Pierre-Malick Boissiere
## Login   <boissi_p@epitech.net>
## 
## Started on  Mon Apr 24 14:37:27 2017 Pierre-Malick Boissiere
## Last update Mon Apr 24 14:40:43 2017 Pierre-Malick Boissiere
##

Hello!
Le programme que j'ai codé sert à comprendre comment gérer les pipes grâces aux commandes
pipe et dup2.
le test se fait sur la commande suivante :
"ls -l | grep -e c"

Je suis actuellement en train de voir comment faire pour coder une fonction récursive
qui pourra gérer n'importe quel nombre de pipe


--> Enfin, je devrais gérer les broken pipes afin qu'ils soient sûrs
(Impossible d'écrire plus de 65 kO dans un pipe).
