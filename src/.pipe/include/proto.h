/*
** proto.h for 42 in /home/boissi_p/rendu/test
** 
** Made by Pierre-Malick Boissiere
** Login   <boissi_p@epitech.net>
** 
** Started on  Wed Apr 19 20:44:51 2017 Pierre-Malick Boissiere
** Last update Wed Apr 19 20:44:52 2017 Pierre-Malick Boissiere
*/

#ifndef PROTO_H_
# define PROTO_H_

char	*epur_str(char *);
int	my_strlen(char *);
void	my_putchar(char);
void	my_putstr(char *);
int	my_getnbr(char *);
void	my_put_nbr(int);
int	my_strcmp(char *, char *);
char	*my_strcpy(char *, char *);
int	my_strncmp(char *, char *, int);
char	*my_strncpy(char *, char *, int);
int	numb_words(char *);
char	**strtowordtab(char *);
char	**create_my_2D(int, int);
void	display_2D(char **);

#endif /* !PROTO_H_ */
