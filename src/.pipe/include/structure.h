/*
** structure.h for 42 in /home/boissi_p/rendu/test
** 
** Made by Pierre-Malick Boissiere
** Login   <boissi_p@epitech.net>
** 
** Started on  Wed Apr 19 20:44:58 2017 Pierre-Malick Boissiere
** Last update Wed Apr 19 20:44:58 2017 Pierre-Malick Boissiere
*/

#ifndef	STRUCTURE_H_
# define STRUCTURE_H_

typedef enum	myCommand
{
  READ = 0,
  WRITE
}		myCommand;

typedef struct	s_list
{
  int	var1;
  char	var2;
  char	sample1;
}		t_list;

#endif /* !STRUCTURE_H_ */
