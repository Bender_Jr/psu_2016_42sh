/*
** my.h for 42 in /home/boissi_p/rendu/test
** 
** Made by Pierre-Malick Boissiere
** Login   <boissi_p@epitech.net>
** 
** Started on  Wed Apr 19 20:44:55 2017 Pierre-Malick Boissiere
** Last update Wed Apr 19 20:44:55 2017 Pierre-Malick Boissiere
*/

# ifndef MY_H_
#  define MY_H_

#include "structure.h"
#include "lib.h"
#include "proto.h"
#include "macrow.h"

#endif /* !MY_H_ */
