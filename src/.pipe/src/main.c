/*
** main.c for 42 in /home/boissi_p/rendu/test
**
** Made by Pierre-Malick Boissiere
** Login   <boissi_p@epitech.net>
**
** Started on  Wed Apr 19 20:45:21 2017 Pierre-Malick Boissiere
** Last update Fri May 19 22:18:32 2017 Bender_Jr
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

/* Ici, je montre comment correctement utiliser pipe*/
int		exec(int ac, char **av, char **env)
{
  int		pipefd[2];
  int		err;
  int		pid;
  int		len;
  char		buff[512];
  int		status;
  int		ret;
  char		**ls;
  char		**grep;

  /* partie initialisation */
  ac = ac;
  av = av;
  ret = 0;
  ls = malloc(sizeof(char *) * 3);
  ls[2] = NULL;
  ls[0] = strdup("ls");
  ls[1] = strdup("-l");
  grep = malloc(sizeof(char *) * 4);
  grep[3] = NULL;
  grep[0] = strdup("grep");
  grep[1] = strdup("-e");
  grep[2] = strdup("c");

  /* début partie intéressante */
  err = pipe(pipefd);
  if (err == -1)
    {
      write(2, "PIPE ERROR\n", 11);
      exit(EXIT_FAILURE);
    }
  if ((pid = fork()) == -1)
    {
      write(2, "FORK ERROR\n", 11);
      exit(EXIT_FAILURE);
    }
  if (pid == 0)
    {
      /* fils */
      close(pipefd[0]);
      err = dup2(pipefd[1], 1);
      if (err == -1)
	{
	  write(2, "DUP2 ERROR\n", 11);
	  exit(EXIT_FAILURE);
	}
      execve("/bin/ls", ls, env);
    }
  e
    lse
    {
      /* père */
      ret = waitpid(pid, &status, 0);
      /* ici on peut checker les valeurs de status */
      close(pipefd[1]);
      err = dup2(pipefd[0], 0);
      if (err == -1)
	{
	  write(2, "DUP2 ERROR\n", 11);
	  exit(EXIT_FAILURE);
	}
      execve("/bin/grep", grep, env);
    }
  return (EXIT_SUCCESS);
}


int		main(int ac, char **av)
{
  exec(ac, av);
  return (0);
}
