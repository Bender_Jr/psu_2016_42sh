/*
** env_check.c for PSU_2016_42sh in /home/bender/Repo/PSU/42sh/rendu/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 18 09:47:23 2017 Bender_Jr
** Last update Sat May 20 09:58:26 2017 Bender_Jr
*/

# include "base.h"

int		is_lval(char c)
{
  int		rt;

  rt = (c >= 'a' && c <= 'z') ? 0 :
    (c >= 'A' && c <= 'Z') ? 0 : -1;
  return (rt);
}

int		check_var(char **args)
{
  size_t	i;

  i = 1;
  while ((i != tab_len(args)))
    {
      if (is_lval(args[i][0]) == -1)
	return (-1);
      if (tab_len(args) < 3)
	i += 1;
      else
	i += 2;
    }
  return (0);
}
