/*
** setenv.c for PSU_2016_42sh in /home/bender/Repo/PSU/42sh/rendu/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sat May 20 09:55:55 2017 Bender_Jr
** Last update Sat May 20 09:58:37 2017 Bender_Jr
*/

# include <stdlib.h>
# include "base.h"

int		err_setenv(int msg)
{
  return (msg == 1 ?
	  (p_printf(2, "setenv: Too many arguments.\n")), (-1) :
	  msg == 2 ?
	  (p_printf(2, "setenv: Variable name must begin with a letter.\n")), (-1) :
	  (-1));
}

int		xsetenv(char **cmd, void *ptr)
{
  int		rt;
  int		argc;
  char		newvar[PATH_MAX];
  t_shell	*type;
  unsigned long	sum;


  type = ptr;
  argc = tab_len(cmd);
  xmemset(newvar, '\0', sizeof(*newvar));
  if (argc > 3 || argc == 1)
    return (argc > 3 ? err_setenv(1) :
	    argc == 1 ? env(cmd, ptr) : 1);
  else if (argc >= 2 && !check_var(cmd))
    {
      sum = get_newvar(argc, cmd, newvar);
      if ((rt = add_or_replace(type->envlist, sum)) == 0)
	type->envlist = fill_env(type->envlist, newvar);
      return (type->envlist = (rt == 1) ?
	      replace_firstnode(type->envlist, newvar) :
	      (rt == 2 ) ? replace_lastnode(type->envlist, newvar) : (rt == 3) ?
	      replace_middlenode(type->envlist, newvar, sum) : type->envlist, 1);
    }
  return (err_setenv(2));
}
