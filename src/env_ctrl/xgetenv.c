/*
** xgetenv.c for PSU_2016_42sh in /home/bender/Repo/PSU/42sh/rendu
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 19 09:31:39 2017 Bender_Jr
** Last update Fri May 19 09:31:48 2017 Bender_Jr
*/

# include "base.h"

char		*xgetenv(const char *name)
{
  size_t	i;
  char		*cpy;
  extern char	**environ;

  i = 0;
  cpy = NULL;
  while (i != tab_len(environ))
    {
      if ((strn_cmp(name, environ[i], len(name))))
	return (cpy = my_strdup(environ[i] + len(name) + 1));
      i++;
    }
  return (cpy);
}
