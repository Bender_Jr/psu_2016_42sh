/*
** env_fctions.c for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sun Apr 23 10:07:11 2017 Bender_Jr
** Last update Sat May 20 09:57:20 2017 Bender_Jr
*/

/*
** for free
*/
# include <stdlib.h>
# include "base.h"

int		xunsetenv(char **cmd, void *ptr)
{
  size_t	i;
  t_shell	*type;
  t_envar	*head_ref;
  t_envar	*to_del;
  unsigned long	cksum;

  i = 1;
  type = ptr;
  head_ref = type->envlist->firstenvar;
  while (cmd[i])
    {
      cksum = get_sum((unsigned char *)cmd[i]);
      if ((to_del = find_node(&head_ref, cksum)))
	del_envar(&head_ref, to_del);
      i++;
    }
  return (i == 1 ? (p_printf(2, "unsetenv: Too few arguments.\n"), -1) : 1);
}

int		env(UNUSED char **cmd, void *ptr)
{
  t_shell	*type;
  t_envar	*tmp;

  type = ptr;
  tmp = type->envlist->firstenvar;
  while (tmp)
    {
      p_printf(1, "%s=%s\n", tmp->variable, tmp->value);
      tmp = tmp->next;
    }
  return (1);
}

void		free_env(t_environ *ptr)
{
  while (ptr->firstenvar)
    {
      free(ptr->firstenvar->variable);
      free(ptr->firstenvar->prev);
      ptr->firstenvar = ptr->firstenvar->next;
    }
  free(ptr->firstenvar);
  free(ptr->lastenvar);
  free(ptr);
}
