/*
** run_exec.c for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sun Apr 23 10:17:57 2017 Bender_Jr
** Last update Sat May 20 11:49:58 2017 Bender_Jr
*/

/*
** for strerror, errno free, excve etc
*/
# include <string.h>
# include <errno.h>
# include <unistd.h>
# include <stdlib.h>
# include "base.h"
# include "get_next_line.h"

int		clean_exit(t_shell *ptr)
{
  freetab(ptr->blts.blts_names);
  free_list(ptr->pathlist);
  close(ptr->history->histfilefd);
  free_history(ptr->history);
  free_env(ptr->envlist);
  reset_cap(&(ptr)->term.save, ptr->term.tty_fd);
  return (g_rt = (g_rt == 1 && g_exit == 1) ? 0 : g_rt);
}

int		is_good_exe(char **argv, t_shell *ptr, t_exec *execl)
{
  errno = 0;
  execl->cksum = get_sum((unsigned char *)argv[0]);
  execl->cmdpath = is_proginlist(ptr->pathlist, execl->cksum);
  if (execl->cmdpath== NULL &&
      (access(argv[0], F_OK | X_OK)) == -1 &&
      (access(execl->cmdpath, X_OK) == -1))
    {
      errno = 0;
      p_printf(2, "%s: Command not found.\n", argv[0]);
      return (-1);
    }
  return (0);
}

int		son_exec(char **argv, char **environ, t_exec *execl)
{
  !access(argv[0], F_OK | X_OK) ? execl->cmdpath = argv[0] : execl->cmdpath;
  if ((execve(execl->cmdpath, argv, environ)))
    {
      errno ? p_printf(2, "%s: %s.\n", argv[0], strerror(errno)) : errno;
      g_rt = 0;
      exit (1);
    }
  return (0);
}

int		fork_failed(t_exec *execl)
{
  if ((execl->parentpid = getpid()) == -1 ||
      (execl->child_pid = fork()) == -1)
    return (-1);
  return (0);
}

int		exec(char **argv, t_shell *ptr)
{
  t_exec	execl;

  g_rt = 0;
  xmemset(&execl, '\0', sizeof(execl));
  if ((is_good_exe(argv, ptr, &execl)) == -1 || fork_failed(&execl))
    return (freetab(argv), -1);
  else if (execl.child_pid == 0)
    son_exec(argv, ptr->environ,  &execl);
  else
    check_status(execl.child_pid, &(execl).status);
  return (freetab(argv), g_rt);
}
