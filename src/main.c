/*
** main.c for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri Apr 14 21:35:39 2017 Bender_Jr
** Last update Sat May 20 10:13:11 2017 Bender_Jr
*/

# include "base.h"

int			main(void)
{
  t_shell		main_list;
  extern char		**environ;

  main_list.term.prompt_frmat = "\033[0m%U@%H \033[1m%~\033[0m >> ";
  fill_builtins(&(main_list).blts);
  if (!tab_len(environ) || xgetenv("PATH") == NULL)
    environ = strto_wordtab(SH_ENV, ",");
  main_list.environ = environ;
  if ((main_list.envlist = init_environ(environ)) == NULL ||
      (main_list.history = init_history()) == NULL ||
      (g_rt = init_term(&(main_list).term)) == -1 ||
      (main_list.pathlist = init_paths(xgetenv("PATH"))) == NULL ||
      (main_list.pathlist = fill_path(main_list.pathlist)) == NULL)
    return (1);
  run(&main_list);
  return (g_rt);
}
