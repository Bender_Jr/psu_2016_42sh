/*
** builtins.c for PSU_2016_42sh in /home/bender/42sh/work/PSU_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Tue Apr 18 17:32:51 2017 Bender_Jr
** Last update Sat May 20 08:13:39 2017 Bender_Jr
*/

/*
** for write and getenv
*/
# include <unistd.h>
# include <stdlib.h>
# include "builtins.h"
# include "base.h"

int		echo(char **cmd, UNUSED void *ptr)
{
  size_t	i;

  i = 1;
  while (cmd[i])
    {
      p_printf(1, "%s ", cmd[i]);
      i++;
    }
  p_printf(1, "\n");
  return (1);
}

int		save_oldpwd(char bfr[])
{
  xmemset(bfr, '\0', PATH_MAX);
  if ((getcwd(bfr, PATH_MAX)) == NULL)
    return (-1);
  return (0);
}

int		cd(char **cmd, UNUSED void *ptr)
{
  char		oldpath[PATH_MAX];

  if (tab_len(cmd) == 1)
    return (((save_oldpwd(oldpath)) == -1 || (chdir(xgetenv("HOME"))) == -1) ?
	    (-1) : (1));
  else if (tab_len(cmd) == 2)
    {
      if (cmd[1][0] != '-' && save_oldpwd(oldpath) == -1)
	return (-1);
      else if (cmd[1][0] == '-' && !cmd[1][1])
	return ((chdir(oldpath) == -1) ? (-1) : (1));
      else if (cmd[1][0] == '-' && cmd[1][1] == '-')
	return (((chdir(xgetenv("HOME")) == -1) ? (-1) : (1)));
      else
	return (chdir(cmd[1]) == -1 ? (-1) : (1));
    }
  p_printf(2, "%s: %s\n", cmd[0], "Too many arguments.");
  return (-1);
}

int		x_exit(char **cmd, UNUSED void *ptr)
{
  if (cmd[1])
    {
      if (!my_stringisnum(cmd[1]))
	return (p_printf(2, "%s: %s", cmd[0], SYNTX_ERR), 1);
      else if ((g_exit = my_atoi(cmd[1])) >= 0 && g_rt <= 255)
	return (g_exit = (g_exit == 0) ? 1 : g_exit);
    }
  return (g_exit = 1);
}

int		clear(UNUSED char **cmd, UNUSED void *ptr)
{
  if (write(1, "\033[H", len("\033[H")) == -1 ||
      write(1, "\033[2J", len("\033[2J")) == -1)
    return (-1);
  return (1);
}
