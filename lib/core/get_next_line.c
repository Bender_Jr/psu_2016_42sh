/*
** get_next_line.c for PSU_2016_42sh in /home/bender/LASTP/psu_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Mon Apr 10 09:34:14 2017 Bender_Jr
** Last update Thu May 18 17:24:45 2017 Bender_Jr
*/

# include <stdlib.h>
# include <unistd.h>
# include "base.h"
# include "get_next_line.h"

static char	*malloc_i_cat(char *dest, char src[],
			      int count, int *pos)
{
  char		*buffer;
  int		k;
  int		i;
  int		j;

  k = 0;
  i = -1;
  j = -1;
  if (dest != NULL)
    while (dest[k] != 0)
      k++;
  if ((buffer = malloc(k + count + 1)) == NULL)
    return (NULL);
  while ((i = i + 1) < k)
    buffer[i] = dest[i];
  while ((j = j + 1) < count)
    buffer[i + j] = src[*pos + j];
  buffer[i + j] = 0;
  if (dest != NULL)
    free(dest);
  *pos = *pos + count + 1;
  return (buffer);
}

char			*is_escape(char *bfr)
{
  unsigned long		sum;

  sum = get_sum((unsigned char *)bfr);
  if (bfr[0] == '\033')
    {
      (sum == get_sum((unsigned char *)"\033[A")) ?
	p_printf(1, "\t...") :
	(sum == get_sum((unsigned char *)"\033[B")) ?
	p_printf(1, "\t...") :
	(sum == get_sum((unsigned char *)"\033[C")) ?
	p_printf(1, "\033[C") :
	(sum == get_sum((unsigned char *)"\033[D")) ?
	p_printf(1, "\033[D") : READ_SIZE;
      xmemset(bfr, '\0', READ_SIZE);
      return( bfr);
    }
  return (bfr);
}

/*
** to put after line 78 if ~ICANON
**  (args.buff[0] != '\033' && isatty(STDIN_FILENO)) ?
**  p_printf(1, "%c", args.buff[0]) : args.i;
*/

char			*get_next_line(const int fd)
{
  char			*line;
  static t_gnl		args;

  line = NULL;
  args.i = 0;
  while (1)
    {
      if (args.j >= args.rd)
	{
	  args.j = 0;
	  if ((args.rd = read(fd, is_escape(args.buff), READ_SIZE)) <= 0 ||
	      xstrchr(args.buff, 4))
	    return (line);
	  args.i = 0;
	}
      if (args.buff[args.j + args.i] == '\n' || args.buff[args.j + args.i] == 4)
	return (malloc_i_cat(line, args.buff, args.i, &(args).j));
      line = (args.j + args.i == args.rd - 1) ?
        malloc_i_cat(line, args.buff, args.i + 1, &(args).j) : line;
      args.i += 1;
    }
  free(line);
  return (NULL);
}
