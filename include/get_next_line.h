/*
** get_next_line.h for PSU_2016_42sh in /home/bender/LASTP/psu_2016_42sh
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Mon Apr 10 09:30:14 2017 Bender_Jr
** Last update Thu May 18 15:22:19 2017 Bender_Jr
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_
/*
** for ssize_t
*/
# include <unistd.h>

#ifndef READ_SIZE
# define READ_SIZE (3)
# endif /* !READ_SIZE */

typedef struct	s_gnl {
  char		buff[READ_SIZE + 1];
  ssize_t	rd;
  int		j;
  int		i;
}		t_gnl;

char	*get_next_line(const int fd);

#endif /* !GET_NEXT_LINE_H_ */
